<!DOCTYPE html>
<html lang="pt-BR">
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
    {{-- <link rel="icon" type="image/png" href="{{asset('img/ico.png')}}"/> --}}
    <title>Kanban</title>
    @inertiaHead
    @routes
  </head>
  <body class="sidebar-noneoverflow bg-light">
    @inertia
    @vite('resources/js/app.js')
  </body>
</html>
