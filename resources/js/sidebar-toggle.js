/*

=========================================================
* Volt Pro - Premium Bootstrap 5 Dashboard
=========================================================

* Product Page: https://themesberg.com/product/admin-dashboard/volt-premium-bootstrap-5-dashboard
* Copyright 2021 Themesberg (https://www.themesberg.com)

* Designed and coded by https://themesberg.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. Please contact us to request a removal. Contact us if you want to remove it.

*/

"use strict";

document.getElementById('sidebar-toggle')
    .addEventListener('click', function () {
        const sidebar = document.getElementById('sidebarMenu');

        if (sidebar.classList.contains('contracted')) {
            sidebar.classList.remove('contracted');
            localStorage.removeItem('sidebar', 'contracted');
        } else {
            sidebar.classList.add('contracted');
            localStorage.setItem('sidebar', 'contracted');
        }
    });
