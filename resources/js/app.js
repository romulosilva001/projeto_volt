import './bootstrap';

import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import { InertiaProgress } from '@inertiajs/progress';

// Vue Toastification
import Toast, { useToast } from 'vue-toastification';
import "vue-toastification/dist/index.css";

// Maska
import Maska from 'maska';

// Import ApexCharts
import VueApexCharts from "vue3-apexcharts";

// Import V Select
import vSelect from 'vue-select';
import "vue-select/dist/vue-select.css";

// Import bootstrap
import 'bootstrap';

// Template Volt
import '../sass/template/volt.scss'

// calendar
import './fullcalendar/main.min.css'
import './fullcalendar/main.min.js'

// Sweet Alert JS & CSS
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

// Leaflet JS & CSS
import leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';

// Import MomentJS
import moment from 'moment';

import 'moment/dist/locale/pt-br.js';

// icons bootstrap
import { BootstrapIconsPlugin } from 'bootstrap-icons-vue';

// Theme CSS Imports
// import '../theme/bootstrap/css/bootstrap.min.css';
// import '../theme/assets/css/plugins.css';
// import '../theme/plugins/perfect-scrollbar/perfect-scrollbar.css';
// import '../css/app.css';
// import '../theme/assets/css/authentication/form-1.css'
// import '../theme/assets/css/forms/theme-checkbox-radio.css';
// import '../theme/assets/css/forms/switches.css';
// import '../theme/assets/css/components/timeline/custom-timeline.css';
// import '../theme/assets/css/apps/scrumboard.css';
// import '../theme/assets/css/elements/custom-typography.css';
// import '../theme/assets/css/elements/color_library.css';

// Theme JS Imports
import '../theme/assets/js/libs/jquery-3.1.1.min.js'
import '../theme/bootstrap/js/bootstrap.bundle.min.js'
import '../theme/assets/js/app.js';
import '../theme/assets/js/authentication/form-1.js';


InertiaProgress.init({
    color: 'white',
    includeCSS: true,
    showSpinner: true,
});

createInertiaApp({
    resolve: (name) => resolvePageComponent(`./pages/${name}.vue`, import.meta.glob('./pages/**/*.vue')),
    setup({ el, App, props, plugin }) {
        createApp({ render: () => h(App, props) })
            .use(plugin)

            .mixin({ methods: { route } })
            .mixin({ data: () => ({ base_url: Ziggy.url }) })
            .mixin({ data: () => ({ L: leaflet, toast: useToast() }) })
            .mixin({ methods: { moment } })
            .use(useToast)
            .use(Maska)
            .use(BootstrapIconsPlugin)
            .use(VueApexCharts)
            .use(VueSweetalert2)
            .use(Toast)
            .component('v-select', vSelect)
            .mount(el)
    },
});

// ação para expandir modal card ao adicionar conteúdos
// $('body').on('hidden.bs.modal', '.modal', function () {
//     $("body").addClass("modal-open");
// });



