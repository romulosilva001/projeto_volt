<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Gerenciamento\GerenciamentoController;
use App\Http\Controllers\TesteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
// Rota para login
Route::controller(AuthController::class)->group(function () {
    Route::get('/', 'showLoginForm')->name('login');
    Route::post('/authenticate', 'authenticate')->name('authenticate');
});




/**
 * Auth Routes
 */
// Rotas acessadas apenas por usuário autenticado
Route::middleware('auth')->group(function () {
    // Rota index - abre o mapa de gerenciamento.levantamento
    Route::controller(GerenciamentoController::class)->group(function () {
        Route::get('/index', 'index')->name('index');
    });

    // Rota para logout do sistema
    Route::controller(AuthController::class)->group(function () {
        Route::get('/logout', 'logout')->name('logout');
    });
});
