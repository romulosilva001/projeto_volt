<?php

namespace App\Http\Controllers\Gerenciamento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class GerenciamentoController extends Controller
{
    public function index()
    {
        return Inertia::render('Index');
    }
}
