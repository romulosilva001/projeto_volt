<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller
{

    public function showLoginForm()
    {
        return Inertia::render('Login');
    }

    public function authenticate(Request $request)
    {
        // dd($request->all());
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            $request->session()->regenerate();
            if (Auth::user()->token_password != null) {
                return Redirect::route('login')->withErrors(['credenciais' => 'Instruções de alterações de senha foram encaminhadas ao email cadastrado!']);
            } else {
                return redirect()->intended('/index');
            }
        }

        return Redirect::route('login')->withErrors([
            'credenciais' => 'Credenciais incorretas! Por favor, verifique suas credenciais e tente novamente',
        ]);
    }


    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
